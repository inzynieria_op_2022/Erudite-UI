/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      screens: {
        'm': '880px',
        // => @media (min-width: 880px) { ... }
      },
    },
  },
  plugins: [],
}
