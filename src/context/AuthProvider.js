import React  from 'react';
import { createContext, useState } from "react";

const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
    const [auth, setAuth] = useState({});
    // const [auth, setAuth] = useState({idUser: 5, user: 'agnieszka1987@gmail.com', key: 'YWduaWVzemthMTk4N0BnbWFpbC5jb206bHViaWVjenl0YWM=', role: 'librarian'});
    // const [auth, setAuth] = useState({idUser: 1, user: 'hubertasztabski@gmail.com', key: 'aHViZXJ0YXN6dGFic2tpQGdtYWlsLmNvbTpQb2xza2FHVVJPTQ==', role: 'client'});

    return (
        <AuthContext.Provider value={{ auth, setAuth }}>
            {children}
        </AuthContext.Provider>
    )
}

export default AuthContext;