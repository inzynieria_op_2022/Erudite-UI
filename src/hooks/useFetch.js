import { useEffect, useState  } from "react";


const useFetch = (url, o) => {
    const [data, setData] = useState(null);
    const [isPending, setIsPending] = useState(true);
    const [error, setError] = useState(null);

    let obj = JSON.parse(o)

    useEffect(() => {
        if(url==='http://localhost:5000/')
        return
        fetch( url, obj )
            .then(res => {
                if (!res.ok) {
                    throw Error('could not fetch the data for that resource');
                } 
                return res.json();
            })
            .then(data => {
                setIsPending(false);
                setData(data);
                setError(null);
            })
            .catch(err => {
                setIsPending(false);
                if(err.message === 'Unexpected end of JSON input')
                    setError(null);
                else    
                    setError(err.message);
            })
    }, [url, o]);

    return {data, isPending, error};
}

export default useFetch;