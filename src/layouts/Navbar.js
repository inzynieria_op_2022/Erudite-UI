import React  from 'react';
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import { useLocation } from 'react-router-dom'
import useAuth from '../hooks/useAuth';

const Navbar = () => {
    
    const { auth, setAuth } = useAuth();
    const path = useLocation().pathname
    const [home, setHome] = useState(path === '/' ? 'slate':'white');
    const [search, setSearch] = useState(path === '/search' ? 'slate':'white');
    const [about, setAbout] = useState(path === '/about' ? 'slate':'white');
    const [contact, setContact] = useState(path === '/contact' ? 'slate':'white');
    const [account, setAccount] = useState(path === '/account' ? 'slate':'white');
    const [newBook, setNewBook] = useState(path === '/new' ? 'slate':'white');
    const [hidden, setHidden] = useState('hidden');
    const [login, setLogin] = useState(false);

    useEffect(() => {
        setHome(path === '/' ? 'slate':'white');
        setSearch(path === '/search' ? 'slate':'white');
        setAbout(path === '/about' ? 'slate':'white');
        setContact(path === '/contact' ? 'slate':'white');
        setAccount(path === '/account' ? 'slate':'white');
        setNewBook(path === '/new' ? 'slate':'white');
        setHidden('hidden')
    },[path]);

    useEffect(() => {
        setLogin(auth?.user ? true : false)
    },[auth]);
    return ( 
        <nav className='sticky top-0 bg-slate-400 p-5 py-7 m:flex m:justify-between'>
            <div className='flex items-center justify-between m:block'>
                <Link to="/" className='flex self-center p-2 text-3xl font-bold bold text-stale-700 '>
                    <span>Erudite</span>
                </Link>
                <div className='m:hidden py-2 px-3' onClick={() => {hidden === 'hidden' ? setHidden('block'):setHidden('hidden')}}>
                    <svg className="w-10 h-10" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
                    </svg>
                </div> 
            </div>
            <div className={`${hidden} m:flex items-center`}>
                <Link to="/">
                    <div className='flex justify-center py-2 px-3 text-stale-700 border-y m:border-y-0 m:border-x border-stone-300'>
                        <svg className="w-6 h-6 mr-1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" >
                            <path strokeLinecap="round" strokeLinejoin="round" d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />
                        </svg>
                        <span>Strona główna</span>
                    </div>
                    <div className={`hidden m:block ml-5 mr-5 h-1 bg-${home}-200 rounded`}></div>
                </Link>
                <Link to="/search">
                    <div className='flex justify-center py-2 px-3 text-stale-700 border-b m:border-r m:border-b-0 border-stone-300'>
                        <svg className="w-6 h-6 mr-1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M15.75 15.75l-2.489-2.489m0 0a3.375 3.375 0 10-4.773-4.773 3.375 3.375 0 004.774 4.774zM21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                        <span>Wyszukaj</span>
                    </div>
                    <div className={`hidden m:block ml-5 mr-5 h-1 bg-${search}-200 rounded`}></div>
                </Link>
                <Link to="/about">
                    <div className='flex justify-center py-2 px-3 text-stale-700 border-b m:border-r m:border-b-0 border-stone-300'>
                        <svg className="w-6 h-6 mr-1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" d="M9.879 7.519c1.171-1.025 3.071-1.025 4.242 0 1.172 1.025 1.172 2.687 0 3.712-.203.179-.43.326-.67.442-.745.361-1.45.999-1.45 1.827v.75M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9 5.25h.008v.008H12v-.008z" />
                        </svg>
                        <span>O nas</span>
                    </div>
                    <div className={`hidden m:block ml-5 mr-5 h-1 bg-${about}-200 rounded`}></div>
                </Link>
                <Link to="/contact" >
                    <div className='flex justify-center py-2 px-3 text-stale-700 border-b m:border-r m:border-b-0 border-stone-300'>
                        <svg className="w-6 h-6 mr-1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" d="M21.75 6.75v10.5a2.25 2.25 0 01-2.25 2.25h-15a2.25 2.25 0 01-2.25-2.25V6.75m19.5 0A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25m19.5 0v.243a2.25 2.25 0 01-1.07 1.916l-7.5 4.615a2.25 2.25 0 01-2.36 0L3.32 8.91a2.25 2.25 0 01-1.07-1.916V6.75" />
                        </svg>
                        <span>Kontakt</span>
                    </div>
                    <div className={`hidden m:block ml-5 mr-5 h-1 bg-${contact}-200 rounded`}></div>
                </Link>
                <div className={`${auth.role==='client' ? 'block':'hidden' }`}>
                <Link to="/account">
                    <div className='flex justify-center py-2 px-3 text-stale-700 border-b m:border-r m:border-b-0 border-stone-300'>
                        <svg className="w-6 h-6 mr-1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z" />
                        </svg>
                        <span>Moje konto</span>
                    </div>
                    <div className={`hidden m:block ml-5 mr-5 h-1 bg-${account}-200 rounded`}></div>
                </Link>
                </div>
                <div className={`${auth.role==='librarian' ? 'block':'hidden' }`}>
                <Link to="/new">
                    <div className='flex justify-center py-2 px-3 text-stale-700 border-b m:border-r m:border-b-0 border-stone-300'>
                        <svg className="w-6 h-6 mr-1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" >
                            <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                        <span>Dodaj książkę</span>
                    </div>
                    <div className={`hidden m:block ml-5 mr-5 h-1 bg-${newBook}-200 rounded`}></div>
                </Link>
                </div>
            </div>
            <div className={`${hidden} m:block`}>
                <div className='flex justify-center self-center  p-2 text-stale-700 border-b m:border-b-0  border-stone-300'>
                    {
                        login
                        ?<Link to='/'>
                            <button onClick={()=>{setAuth({})}} className='bg-slate-500 hover:bg-slate-700 text-white font-bold py-2 px-4 rounded-full'>Wyloguj się</button>
                        </Link> 
                        : <Link to="/login" >
                            <button className='bg-slate-500 hover:bg-slate-700 text-white font-bold py-2 px-4 rounded-full'>Zaloguj się</button>
                        </Link>
                    }
                </div>
                
            </div>
        </nav>
     );
}
 
export default Navbar;