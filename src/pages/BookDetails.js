import React  from 'react';
import { useParams, Link } from 'react-router-dom';
import { useState, useEffect, useRef } from "react";
import useAuth from '../hooks/useAuth';

import Button from '../components/Button';
import useFetch from '../hooks/useFetch';

const BookDetails = () => {

    const { auth, setAuth } = useAuth();
    const [ method, setMethod] = useState('GET')
    const { id } = useParams();
    const notInitialRender = useRef(false)
    const [url, setUrl] = useState('/books/'+id)
    const [book, setBook] = useState(null)
    const [catalog, setCatalog] = useState(null)
    const [message,setMessage] = useState(null)
    const object = {
        method: method,
        headers: {
            Authorization: 'Basic ' + auth.key
        },
    }
    const { data, error, isPending } = useFetch(`http://localhost:5000${url}`, JSON.stringify(object));
    
    useEffect(()=>{setMessage(error)},[error])
    useEffect(() => {
        if (notInitialRender.current) {
            if(book === null){
                setBook(data)
                if(auth.role === 'client'){
                    setMessage(null)
                    setUrl('/catalog/get_for_book?id_book='+id)
                }
                    
                return
            }
            else if(catalog === null){
                setCatalog(data)
                return
            }
        } else {
        notInitialRender.current = true
        }
    }, [data])
    function reservation(id) {
        if(auth.role === 'client'){
            setMessage(null)
            setMethod('POST')
            setUrl('/catalog/'+id+'/reserve')
        }
    }
    return (  
        <main className='p-5 '>
            { message && <div className='text-red-700'>{ message }</div> }
            { isPending && <div>Loading...</div> }
            { auth.role === 'librarian'? <Link to={`/book/${id}/edit`} className='absolute top-1/4 right-0 mr-2'><Button name='Edit' fun={()=>{}}/></Link>:''}
            { book && 
                <section className='w-full grid grid-cols-1 m:grid-cols-2'>
                    <img src={ book.cover === null ? require('../assets/gfx/book.png'):book.cover } alt="Book"  className='flex justify-self-center w-4/5' style={{
                        height: '300px',
                        objectFit: 'cover',
                        width: 'auto'
                    }}></img>
                    <div className='p-5 w-4/5'>
                        <p className='text-2xl text-stale-700 font-bold self-start m-2'>{ book.title }</p>
                        <div className='text-stale-500 flex justify-between'>
                            <span className='font-medium'>{`${book.authors.length === 1 ? 'Autor':'Autorzy:'}`}</span>
                            <div >
                                {book.authors.map(author => (
                                    <p key={ author.id_author }>{ author.name }</p>
                                ))} 
                            </div>
                        </div>
                        <p className='text-stale-500 flex justify-between'>
                            <span className='font-medium'>Wydawnictwo: </span> 
                            <span>{ book.publisher.name }</span>
                        </p>
                        <p className='text-stale-500 flex justify-between'>
                            <span className='font-medium'>Rok wydania: </span> 
                            <span>{ book.year }</span>
                        </p>
                        <p className='text-stale-500 flex justify-between'>
                            <span className='font-medium'> Liczba stron:</span> 
                            <span>{ book.pages }</span>
                        </p>
                        <p className='text-stale-500 flex justify-between'>
                            <span className='font-medium'>ISBN: </span> 
                            <span>{ book.isbn }</span>
                        </p>
                    </div>
                    <div className='m:col-span-2 w-full p-2' >
                        <p className='text-stale-500 flex justify-items-center align-end'>
                            <span>{ book.description }</span>
                        </p>
                    </div>
                    <div className='m:col-span-2 w-full p-2 flex flex-wrap '>
                        {book?.tags.map(tag => (
                            <p key={ tag.id_tag } style={{backgroundColor: tag.color}} className='m-2 p-2 rounded'>{ tag.name }</p>
                        ))} 
                    </div>
                    {catalog && <div>
                        <p>Książki dostępne do rezerwacji:</p>
                        {catalog?.map(c => (
                            <div key={ c.id_catalog } className='border-2 flex p-2 m-1 justify-between '>
                                <p className='p-2'>Sygnatura: { c.shelf_mark }</p>
                                <Button name='Rezerwuj' fun={reservation} data={c.id_catalog}/>
                            </div>
                        ))}
                    </div>}
                </section>
            }
        </main>
    );
}
 
export default BookDetails;