import React  from 'react';
import { useState, useEffect } from "react";
import { useParams, Navigate } from 'react-router-dom';
import useFetch from '../hooks/useFetch';
import useAuth from '../hooks/useAuth';


import Input from '../components/Input';
import Button from '../components/Button';
import InputList from '../components/InputList';



const EditBook = () => {
    const { id } = useParams();
    const { auth, setAuth } = useAuth();

    const [url, setUrl] = useState(`/books/${id}`)
    const [object,setObject] = useState({
        method: 'GET',
    })
    const { data, isPending, error } = useFetch(`http://localhost:5000${url}`, JSON.stringify(object))

    const [listAuthor, setListAuthor] = useState(null)
    const [listPublisher, setListPublisher] = useState(null)
    const [listTags, setListTags] = useState(null)
    const [book, setBook] = useState(null)
    const [title, setTitle] = useState(null)
    const [author, setAuthor] = useState(null)
    const [authorId, setAuthorId] = useState(null)
    const [isbn, setIsbn] = useState(null)
    const [pages, setPages] = useState(null)
    const [year, setYear] = useState(null)
    const [publisher, setPublisher] = useState(null)
    const [publisherId, setPublisherId] = useState(null)
    const [description, setDescription] = useState(null)
    const [tags, setTags] = useState(null)
    const [tagsId, setTagsId] = useState(null)
    const [message,setMessage] =useState(null)
    const [succes,setSucces] =useState(null)
    const [newId,setnewId] = useState(null)

    useEffect(()=>{setMessage(error)},[error])
    useEffect(()=>{
        if(book === null){
            setBook(data)
            setListAuthor(null)
            setListPublisher(null)
            setListTags(null)
            setTitle(data?.title)
            setAuthor(data?.authors.map(el => (' '+el.name)))
            setAuthorId(data?.authors.map(el => (el.id_author)))
            setIsbn(data?.isbn)
            setPages(data?.pages)
            setYear(data?.year)
            setPublisher(data?.publisher.name)
            setPublisherId(data?.publisher.id_publisher)
            setDescription(data?.description)
            setTags(data?.tags.map(el => (' '+el.name)))
            setTagsId(data?.tags.map(el => (el.id_tag)))
        }else if(url === '/authors'){
            setMessage(null)
            setListAuthor(data)
        }else if(url === '/publishers'){
            setMessage(null)
            setListPublisher(data)
        }else if(url === '/tags'){
            setMessage(null)
            setListTags(data)
        }
    },[data])


    function handleSubmit(x) {
        let a=[]
        let t=[]
        for(let i=0;i<authorId.length;i++){
            if(!a.includes(authorId[i]))
                a.push(authorId[i])
        }
        for(let i=0;i<tagsId.length;i++){
            if(!t.includes(tagsId[i]))
                t.push(tagsId[i])
        }
        let schema = {
            "title": title,
            "authors": a,
            "isbn": isbn,
            "pages": parseInt(pages),
            "year": parseInt(year),
            "id_publisher": publisherId,
            "description": description,
            "tags": t,
            
        }
        setObject({
            method: 'PUT',
            headers: { 
                'Content-Type': 'application/json',
                Authorization: 'Basic ' + auth.key
            },
            body: JSON.stringify(schema)
        })
        setMessage(null)
        setUrl('/books/'+id)
    }

    useEffect(()=>{
        if(url==='/books' && isPending===false && error===null){
            setnewId(data.id_book)
            setSucces(true)
        }
    },[data])

    function clickInput(u){
        if(u!=='/authors')
            setListAuthor(null)
        if(u!=='/publishers')
            setListPublisher(null)
        if(u!=='/tags')
            setListTags(null)
        if(u ==='/tags' || u==='/authors'|| u==='/publishers'){
            setMessage(null)
            setUrl(u)
        }
            
    }
    function clickInputList(t,i,n) {
        if(t === 'authors'){
            setAuthor(author+', '+n)
            let x=authorId
            x.push(i)
            setAuthorId(x)
        }
        if(t === 'publishers'){
            setPublisher(n)
            setPublisherId(i)
        }
        if(t === 'tags'){
            setTags(tags+', '+n)
            let x=tagsId
            x.push(i)
            setTagsId(x)
        }
    }

    return (  
        <main className='p-5 flex justify-center' >
            { message && <div className='text-red-700'>{ message }</div> }
            { succes && <Navigate to={'/book/'+ newId} replace={true} />}
            {book && <section className='w-1/2'>
                <Input type='text' label='Tytuł' name='title' value={title} set={setTitle} fun={clickInput}/>
                <Input readonly='readonly' type='textarea' label='Autor/Autorzy' name='authors' value={author} set={setAuthor} fun={clickInput}/>
                {listAuthor && <div>
                    {listAuthor?.map(el => (
                        <InputList key={el.id_author} name={el.name} type='authors' id={el.id_author} fun={clickInputList}/>
                    ))}    
                </div>}
                <Input type='text' label='ISBN' name='isbn' value={isbn} set={setIsbn} fun={clickInput}/>
                <Input type='number' label='Liczba stron' name='pages' value={pages} set={setPages} fun={clickInput}/>
                <Input type='number' label='Rok wydania' name='year' value={year} set={setYear} fun={clickInput}/>
                <Input readonly='readonly' type='text' label='Wydwanictwo' name='publishers' value={publisher} set={setPublisher} fun={clickInput}/>
                {listPublisher && <div>
                    {listPublisher?.map(el => (
                        <InputList key={el.id_publisher} name={el.name} type='publishers' id={el.id_publisher} fun={clickInputList}/>
                    ))}    
                </div>}
                <Input type='textarea' label='Opis' name='description' value={description} set={setDescription} fun={clickInput}/>
                <Input readonly='readonly' type='textarea' label='Tagi' name='tags' value={tags} set={setTags} fun={clickInput}/>
                {listTags && <div>
                    {listTags?.map(el => (
                        <InputList key={el.id_tag} name={el.name} type='tags' id={el.id_tag} fun={clickInputList}/>
                    ))}    
                </div>}
                <Button name='Zatwierdź' fun={handleSubmit}/>
            </section>}
            
        </main>

    );
}
 
export default EditBook;
