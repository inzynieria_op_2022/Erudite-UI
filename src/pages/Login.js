import React  from 'react';
import { useState, useEffect, useContext } from "react";
import { Link, Navigate } from "react-router-dom";
import useAuth from '../hooks/useAuth.js'
import useFetch from '../hooks/useFetch';



const Login = () => {

    const { auth, setAuth } = useAuth();
    const [user, setUser] = useState('');
    const [pwd, setPwd] = useState('');
    const [ key, setKey ] = useState('')
    const [url,setUrl] =useState('/')
    const [message,setMessage] = useState(null)
    const [succes,setSucces] =useState(null)


    const object = {
        method: 'GET',
        headers: {
            Authorization: 'Basic ' + key
        },
    }

    const { data, isPending, error } = useFetch('http://localhost:5000'+url, JSON.stringify(object))


    const handleSubmit = async (e) => {
        e.preventDefault();
        if(user !== '' && pwd !== ''){
            setKey(window.btoa( user+':'+pwd))
            setUrl('/auth/check')
        }
    }
    useEffect(()=>{setMessage(error)},[error])

    useEffect(()=>{
        let idUser = data?.id_user
        let role = data?.role
        setAuth({ idUser, user, key, role })
    },[data])

    useEffect(()=>{
        setUser('')
        setPwd('')
        if(auth.role !== undefined){
            setSucces(true)
        }
    },[auth])

    return (  
        <main className='p-5 '>
            { message && <div className='text-red-700'>{ message }</div> }
            { succes && <Navigate to={'/account'} replace={true} />}

            <form onSubmit={handleSubmit} className="flex flex-col items-center m-5 mt-10">
                <label className='w-4/5 flex items-center justify-between text-slate-700'>
                    <span className='font-bold'>Login</span>
                    <input 
                        type='text' 
                        name='user'
                        onChange={(e) => setUser(e.target.value)}
                        value={user} 
                        className='m-1 block w-10/12 bg-white border border-slate-200 rounded-md p-2 shadow-sm focus:outline-none focus:border-slate-300 focus:ring-slate-200 focus:ring-1 m:text-base text-xs'
                    />
                </label>
                <label className='w-4/5 flex items-center justify-between text-slate-700'>
                    <span className='font-bold'>Hasło</span>
                    <input 
                        type='password' 
                        name='pwd'
                        onChange={(e) => setPwd(e.target.value)}
                        value={pwd} 
                        className='m-1 block w-10/12 bg-white border border-slate-200 rounded-md p-2 shadow-sm focus:outline-none focus:border-slate-300 focus:ring-slate-200 focus:ring-1 m:text-base text-xs'
                    />
                </label>
                <input 
                    type='submit' 
                    value='Zaloguj się' 
                    className='m-1 block w-1/4 bg-white border border-slate-200 rounded-md p-2 shadow-sm focus:outline-none focus:border-slate-300 focus:ring-slate-300 focus:ring-1 sm:text-slate-700 ' 
                />
            </form>
            <p>Nie masz konta?</p>
            <Link to="/register">
                <button className='bg-slate-500 hover:bg-slate-700 text-white font-bold py-2 px-4 rounded-full'>Zarejestruj się</button> 
            </Link>
        </main>
    );
}
export default Login;