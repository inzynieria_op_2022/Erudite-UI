import React, { useState, useEffect }  from 'react';
import { Link } from "react-router-dom";
import BookList from '../components/BookList';
import useFetch from '../hooks/useFetch';

const Home = () => {

    const object = {
        method: 'GET',
    }
    const { data, isPending, error } = useFetch('http://localhost:5000/books', JSON.stringify(object))
    const [ search, setSearch ] = useState('');
    const [message,setMessage] = useState(null)

    useEffect(()=>{setMessage(error)},[error])

    return ( 
        <main className='p-5 '>
            <form className="flex justify-center align-center w-full m-5">
                <input 
                    placeholder='Search for anything...' 
                    type='text' 
                    name='search' 
                    className='w-3/5 m-1 placeholder:text-slate-400 block bg-white border border-slate-200 rounded-md py-2 pl-2 m:pl-9 shadow-sm focus:outline-none focus:border-slate-300 focus:ring-slate-200 focus:ring-1 m:text-base text-xs' 
                    onChange={(e) => setSearch(e.target.value)}
                    value={search}
                    />
                <Link to={`/search?param=${search}`} className='m-1 block w-1/4' >
                    <input 
                        type='submit' 
                        value='Szukaj' 
                        className='w-4/5 block bg-white border border-slate-200 rounded-md py-2 px-1 m:pl-2 shadow-sm focus:outline-none focus:border-slate-300 focus:ring-slate-300 focus:ring-1 sm:text-sm  m:text-base sm:text-slate-700 text-xs' 
                    />               
                </Link>
            </form>
            <section  className='flex justify-center items-center'>
                { message && <div className='text-red-700'>{ message }</div> }
                { isPending && <div>Loading...</div> }
                { data && 
                <BookList data={ data } display={ true } button={''}/>
                }
            </section>
            
        </main>
        
    );
}
 
export default Home;