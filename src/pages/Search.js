import React  from 'react';
import { useState, useEffect } from "react";
import BookList from '../components/BookList';
import useFetch from '../hooks/useFetch';

const Search = () => {

    var href = new URL(window.location.href);
    var param = href.searchParams.get("param");

    const object = {
        method: 'GET',
    }
    const [ url, setUrl ] = useState(`http://localhost:5000/books/search?s=${param}`);

    const { data, isPending, error } = useFetch(url,JSON.stringify(object));
    
    const [title, setTitle] = useState('');
    const [author, setAuthor] = useState('');
    const [isbn, setIsbn] = useState('');
    const [message,setMessage] = useState(null)

    useEffect(()=>{setMessage(error)},[error])

    const handleSubmit = async (e) => {
        e.preventDefault();
        setUrl(`http://localhost:5000/books/search?title=${title}&author=${author}&isbn=${isbn}`)
    }
    return (  
        <main className='p-5'>
            { message && <div className='text-red-700'>{ message }</div> }
            <form className="flex flex-col items-center m-5 mt-10" onSubmit={handleSubmit}>
                <label className='w-4/5 flex items-center justify-between text-slate-700'>
                    <span className='font-bold'>Tytuł</span>
                    <input 
                        type='text' 
                        name='title'
                        onChange={(e) => setTitle(e.target.value)}
                        value={title} 
                        className='m-1 block w-10/12 bg-white border border-slate-200 rounded-md p-2 shadow-sm focus:outline-none focus:border-slate-300 focus:ring-slate-200 focus:ring-1 m:text-base text-xs'
                    />
                </label>
                <label className='w-4/5 flex items-center justify-between text-slate-700'>
                    <span className='font-bold'>Autor</span>
                    <input 
                        type='text' 
                        name='author'
                        onChange={(e) => setAuthor(e.target.value)}
                        value={author}  
                        className='m-1 block w-10/12 bg-white border border-slate-200 rounded-md p-2 shadow-sm focus:outline-none focus:border-slate-300 focus:ring-slate-200 focus:ring-1 m:text-base text-xs' 
                    />
                </label>
                <label className='w-4/5 flex items-center justify-between text-slate-700'>
                    <span className='font-bold'>ISBN</span>
                    <input 
                        type='text' 
                        name='isbn'
                        onChange={(e) => setIsbn(e.target.value)}
                        value={isbn}  
                        className='m-1 block w-10/12 bg-white border border-slate-200 rounded-md p-2 shadow-sm focus:outline-none focus:border-slate-300 focus:ring-slate-200 focus:ring-1 m:text-base text-xs' 
                    />
                </label>
                <input 
                    type='submit' 
                    value='Szukaj' 
                    className='m-1 block w-1/4 bg-white border border-slate-200 rounded-md p-2 shadow-sm focus:outline-none focus:border-slate-300 focus:ring-slate-300 focus:ring-1 sm:text-slate-700 ' 
                />
            </form>
            <section  className='w-full flex justify-center items-center'>
                { error && <div className='text-red-700'>{ error }</div> }
                { isPending && <div>Loading...</div> }
                { data && 
                <BookList data={ data } display={ false } button={null}/>
                }
            </section>
        </main>
    );
}
 
export default Search;