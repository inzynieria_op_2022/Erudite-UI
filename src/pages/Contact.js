const Contact = () => {
    return (  
        <main className='p-5 '>
             <form className="flex flex-col items-center m-5 mt-10">
                <label className='w-4/5 flex flex-col items-start text-slate-700'>
                    <span className='font-bold'>Podaj email odezwiamy się do ciebie.</span>
                    <input 
                        type='text' 
                        name='email'
                        className='m-1 block w-full bg-white border border-slate-200 rounded-md p-2 shadow-sm focus:outline-none focus:border-slate-300 focus:ring-slate-200 focus:ring-1 m:text-base text-xs' />
                </label>
                <input type='submit' value='Wyślij' className='m-1 block w-1/4 bg-white border border-slate-200 rounded-md p-2 shadow-sm focus:outline-none focus:border-slate-300 focus:ring-slate-300 focus:ring-1 sm:text-slate-700 ' />
            </form>
        </main>
    );
}
 
export default Contact;