import React  from 'react';
import { useState, useEffect } from "react";
import useFetch from '../hooks/useFetch';
import useAuth from '../hooks/useAuth';

const Account = () => {

    const [reservation, setReservation] = useState(true);
    const [loan, setLoan] = useState(false);
    const [button, setButton] = useState('');
    const [method, setMethod] = useState('GET')
    const [booksList, setBooksList] = useState(null)
    const [message,setMessage] = useState(null)

    const { auth, setAuth } = useAuth();

    const object = {
        method: method,
        headers: {
            Authorization: 'Basic ' + auth.key
        },
    }
    const [ url, setUrl ] = useState('/reservations/my_reservations');
    const { data, isPending, error } = useFetch(`http://localhost:5000${url}`, JSON.stringify(object))

    useEffect(()=>{
        if(booksList===null){
            setBooksList(data)
        }
    },[data])
    useEffect(()=>{
        setMessage(error)
    },[error])
    return (  
        <main className='p-5 '>
            <section className='flex justify-center text-slate-700'>
                <p onClick={()=>{setMessage(null);setMethod('GET');setBooksList(null);setReservation(true);setLoan(false);setButton('Rezygnuj');setUrl('/reservations/my_reservations')}} className={`${reservation ? 'opacity-100 font-bold':'opacity-50'} bg-slate-200 m-2 p-2 shadow-md rounded hover:shadow-lg`}>Książki zarezerwowane</p>
                <p onClick={()=>{setMessage(null);setMethod('GET');setBooksList(null);setReservation(false);setLoan(true);setButton('Prolonguj');setUrl('/loans/my_loans')}} className={`${loan ? 'opacity-100 font-bold':'opacity-50'} bg-slate-200 m-2 p-2 shadow-md rounded hover:shadow-lg`}>Ksiązki wypożyczone</p>
            </section>
            <section  className='flex flex-col justify-center items-center'>
                { message && <div className='text-red-700'>{ message }</div> }
                { isPending && <div>Loading...</div> }
                { booksList && 
                    booksList?.map(book => (
                        <div key={ book?.id_book } className='w-full flex justify-center'>
                            <div className='w-3/4 flex flex-col sm:flex-row justify-between m-2 p-2 bg-slate-200 shadow-md rounded hover:shadow-lg'>
                                <img src={ book.cover === null ? require('../assets/gfx/book.png'):book.cover } alt="Book" className='' style={{
                                    height: '200px',
                                    objectFit: 'cover',
                                    width: 'auto'
                                }}></img>                        
                                <div className='flex flex-col items-start justify-end p-5' >
                                    <span className='text-stale-700 font-bold self-start'>{ book.title }</span>
                                    {book.authors.map(author => (
                                        <span className='text-xs text-stale-500 self-start' key={ author.id_author }>Autor: { author.name }</span>
                                    ))}
                                </div>
                            </div>
                            <div className='m-10'>
                                <button 
                                    className='bg-slate-500 m-2 hover:bg-slate-700 text-white font-bold py-2 px-4 rounded-full' 
                                    onClick={()=>{
                                        if(reservation){
                                            setMessage(null)
                                            setMethod('POST')
                                            setUrl('/reservations/'+book?.id_reservation+'/loan')
                                            if(error === null)
                                                setMessage('Książka przeniesona do listy wypożyczone')
                                            let b = []
                                            for(let i=0;i<booksList.length;i++){
                                                if(booksList[i].id_book !== book?.id_book){
                                                    b.push(booksList[i])
                                                }
                                            }
                                            setBooksList(b)
                                        }else{
                                            setMessage(null)
                                            setMethod('POST')
                                            setUrl('/loans/'+book?.id_loan+'/prolong')
                                            if(error === null)
                                                setMessage('Termin książki został przedłużony o 2 miesiące ')
                                        }
                                    }}
                                >
                                    {reservation? 'Książka odebrana':'Prolonguj'}
                                </button>
                                <button 
                                    className='bg-slate-500 m-2 hover:bg-slate-700 text-white font-bold py-2 px-4 rounded-full'
                                    onClick={()=>{
                                        if(reservation){
                                            setMessage(null)
                                            setMethod('DELETE')
                                            setUrl('/reservations/'+book?.id_reservation)
                                            if(error === null)
                                                setMessage('Usinięto książkę z listy rezerwacji')
                                        }else{
                                            setMessage(null)
                                            setMethod('DELETE')
                                            setUrl('/loans/'+book?.id_loan)
                                            if(error === null)
                                                setMessage('Usinięto książkę z listy wypożyczonych')
                                        }
                                        let b = []
                                        for(let i=0;i<booksList.length;i++){
                                            if(booksList[i].id_book !== book?.id_book){
                                                b.push(booksList[i])
                                            }
                                        }
                                        setBooksList(b)
                                    }}
                                >
                                    {reservation? 'Usuń rezerwacje':'Książka oddana'}
                                </button>
                            </div>
                        </div>
                    ))
                }
            </section>
        </main>
    );
}
 
export default Account;