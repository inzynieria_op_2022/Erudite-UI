const About = () => {
    return (  
        <main className="p-5">
            <span className="m-2">Library Management System (LMS) jest oprogramowaniem ułatwiającym prowadzenie bibliotek. Osoby zarządzające zbiorem książek muszą być w stanie śledzić swoją kolekcję, znać fizyczne położenie każdej książki oraz wiedzieć kto, jaką książkę wypożyczył. Poza tym, korzystając z możliwości, jakie daje rewolucja informacyjna, biblioteki są teraz w stanie dać możliwość użytkownikom zdalnego założenia konta na swojej stronie internetowej i za jej pośrednictwem rezerwowania książek do odebrania i otrzymywania powiadomień o kończącym się terminie wypożyczenia książki. W nadchodzących miesiącach nasz zespół będzie starał się stworzyć przynajmniej podstawową wersję takiego systemu.</span>
            <p className="m-2 mt-5"> Podział obowiązków</p>
            <p className="m-2">Jan Karkowski - frontend</p>
            <p className="m-2">Hubert Asztabski - backend, testy</p>
            <p className="m-2">Stanisław Borowy - backend, dokumentacja</p>
            <p className="m-2">Jakub Głowacki - backend, narzędzia informatyczne</p>
        </main>

    );
}
 
export default About;
