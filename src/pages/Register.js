import React  from 'react';
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import useFetch from '../hooks/useFetch';
import Input from '../components/Input';
import Button from '../components/Button';


const Register = () => {

    const [email, setEmail] = useState('');
    const [pwd, setPwd] = useState('');
    const [match, setMatch] = useState('');
    const [ url, setUrl ] = useState('/');
    const [message,setMessage] = useState(null)
    const [obj,setObj] = useState(null)

    const object = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(obj)
    }

    const { data, isPending, error } = useFetch('http://localhost:5000'+url, JSON.stringify(object))

    useEffect(()=>{setMessage(error)},[error])

    function handleSubmit(){
        if(pwd === match){
            
            setEmail('')
            setPwd('');
            setMatch('');
            setObj(
                {
                    email:email,
                    password:pwd,
                }
            )
            setUrl('/auth/register')
        }else{
            setMessage('Brak zgodności haseł')
        }
    }

    return (  
        <main className='p-5 '>
            { message && <div className='text-red-700'>{ message }</div> }
                <Input type='text' label='Email' name='email' value={email} set={setEmail} fun={()=>{}}/>
                <Input type='password' label='Hasło' name='pwd' value={pwd} set={setPwd} fun={()=>{}}/>
                <Input type='password' label='Powtórz hasło' name='match' value={match} set={setMatch} fun={()=>{}}/>
                <Button name='Zatwierdź' fun={handleSubmit}/>
            <p>Masz już konto?</p>
            <Link to="/login">
                <button className='bg-slate-500 hover:bg-slate-700 text-white font-bold py-2 px-4 rounded-full'>Zaloguj się</button> 
            </Link>
        </main>
    );
}
 
export default Register;