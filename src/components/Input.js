import React  from 'react';
import { useState, useEffect } from "react";


const Input = (props) => {

    const label = props?.label;
    const name = props?.name;
    const set = props?.set;
    const value = props?.value;
    const fun = props?.fun;
    const type = props?.type;
    const readonly = props?.readonly


    return ( 
        <label className='m-2 flex flex-col items-start text-slate-700'>
            <span className='mb-1 font-bold'>{label}</span>
            {type==='textarea'
            ? <textarea
                readOnly={readonly}
                type={type}
                name={name}
                onChange={(e) => set(e.target.value)}
                value={value} 
                className=' w-full bg-white border border-slate-200 rounded-md p-2 shadow-sm focus:outline-none focus:border-slate-300 focus:ring-slate-200 focus:ring-1 m:text-base text-xs'
                onFocus={()=>fun('/'+name)}
            />
            :<input
                type={type}
                name={name}
                onChange={(e) => set(e.target.value)}
                value={value} 
                className=' w-full bg-white border border-slate-200 rounded-md p-2 shadow-sm focus:outline-none focus:border-slate-300 focus:ring-slate-200 focus:ring-1 m:text-base text-xs'
                onFocus={()=>fun('/'+name)}
            />
            }
        </label>
    );
}
 
export default Input;