import React  from 'react';
import { Link } from "react-router-dom";

const BookListElement = (props) => {

    const book = props.book;
    const display = props.display;
    const button = props.button;
    return (
        <>
            {display && 
            <Link to={`/book/${book.id_book}`}>
                <div className='flex flex-col items-center  m-2 p-2 bg-slate-200 shadow-md rounded hover:shadow-lg'>
                    <img src={ book.cover === null ? require('../assets/gfx/book.png'):book.cover } alt="Book" className='' style={{
                        height: '400px',
                        objectFit: 'cover',
                        width: 'auto'
                    }}></img>
                    <span className='text-stale-700 font-bold self-start'>{ book.title }</span>
                    {book.authors.map(author => (
                        <span className='text-xs text-stale-500 self-start' key={ author.id_author }>{ author.name }</span>
                    ))}
                    
                </div>
            </Link>
            }
            {!display && 
            <Link to={`/book/${book.id_book}`}>
                <div className='w-full flex flex-col sm:flex-row justify-between m-2 p-2 bg-slate-200 shadow-md rounded hover:shadow-lg'>
                <img src={ book.cover === null ? require('../assets/gfx/book.png'):book.cover } alt="Book" className='' style={{
                    height: '200px',
                    objectFit: 'cover',
                    width: 'auto'
                }}></img>                        
                <div className='flex flex-col items-start justify-end p-5' >
                        <span className='text-stale-700 font-bold self-start'>{ book.title }</span>
                        {book.authors.map(author => (
                            <span className='text-xs text-stale-500 self-start' key={ author.id_author }>Autor: { author.name }</span>
                        ))}
                    </div>
                </div>
            </Link>
            }
            
        </>
        
    );
}
 
export default BookListElement;