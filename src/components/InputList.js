import React  from 'react';

const InputList = (props) => {

    const name = props?.name;
    const id = props?.id
    const type = props?.type
    const fun = props?.fun
    return ( 
        <p onClick={()=>fun(type,id,name)} className='w-1/2 m-2 bg-slate-100 border border-slate-200 rounded-md p-2 shadow-sm hover:shadow-lg m:text-base '>
            {name}
        </p>
    );
}
 
export default InputList;