import React  from 'react';
import BookListElement from './BookListElement';

const BookList = (props) => {

    const data = props.data;
    const display = props.display;
    const button = props.button

    return ( 
        <div className={`grid grid-cols-1 ${display ? 'sm:grid-cols-2 m:grid-cols-3':'sm:grid-cols-1'} m-5`}>
            {data.map(book => (
                <BookListElement book={book} key={ book?.id_book } display={display} button={button}/>
            ))}
        </div>
    );
}
 
export default BookList;