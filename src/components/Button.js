import { data } from 'autoprefixer';
import React  from 'react';
import { useState, useEffect } from "react";


const Button = (props) => {

    const name = props?.name;
    const fun = props?.fun
    const data=props?.data
    return ( 
        <button onClick={()=>fun(data)} className='bg-slate-500 m-2 hover:bg-slate-700 text-white font-bold py-2 px-4 rounded-full'>{name}</button> 
    );
}
 
export default Button;