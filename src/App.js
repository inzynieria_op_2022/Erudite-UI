import React  from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Navbar from './layouts/Navbar'
import Home from './pages/Home'
import Search from './pages/Search';
import About from './pages/About';
import Contact from './pages/Contact';
import BookDetails from './pages/BookDetails';
import Login from './pages/Login';
import Register from './pages/Register';
import Account from './pages/Account';
import NewBook from './pages/NewBook';
import EditBook from './pages/EditBook';

function App() {
  return (
    <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/search" element={<Search />} />
          <Route path="/about" element={<About />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/book/:id" element={<BookDetails />} />
          <Route path="/book/:id/edit" element={<EditBook />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/account" element={<Account />} />
          <Route path="/new" element={<NewBook />} />
        </Routes>


    </BrowserRouter>
  );
}

export default App;
