## Installation in development environment
In order to install and run Erudite UI run
```
npm install
npm start
```
## Installation in production environment
For instructions on how to install react app in production
environment see [https://create-react-app.dev/docs/deployment/](https://create-react-app.dev/docs/deployment/).
